﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WsPromoSSLActivator.Helpers
{
    public class cTripleDes
    {
        private TripleDESCryptoServiceProvider m_des = new TripleDESCryptoServiceProvider();
        private UTF8Encoding m_utf8 = new UTF8Encoding();
        private byte[] m_key;
        private byte[] m_iv;

        public cTripleDes(byte[] m_key, byte[] m_iv)
        {
            this.m_key = m_key;
            this.m_iv = m_iv;
        }
        public byte[] Encrypt(byte[] input)
        {
            return Transform(input, m_des.CreateEncryptor(m_key, m_iv));
        }
        public byte[] Decrypt(byte[] input)
        {
            return Transform(input, m_des.CreateDecryptor(m_key, m_iv));
        }
        public string Encrypt(string text)
        {
            byte[] input = m_utf8.GetBytes(text);
            byte[] output = Transform(input, m_des.CreateEncryptor(m_key, m_iv));
            return System.Convert.ToBase64String(output);
        }
        public string Decrypt(string text)
        {
            byte[] input = System.Convert.FromBase64String(text);
            byte[] output = Transform(input, m_des.CreateDecryptor(m_key, m_iv));
            return m_utf8.GetString(output);
        }

        private byte[] Transform(byte[] input, ICryptoTransform CryptoTransform)
        {
            //Create the necessary streams
            MemoryStream memStream = new MemoryStream();
            CryptoStream cryptStream = new CryptoStream(memStream, CryptoTransform, CryptoStreamMode.Write);

            // Transform the bytes as requested
            cryptStream.Write(input, 0, input.Length);
            cryptStream.FlushFinalBlock();

            // Read the memory stream and convert it back into byte array
            memStream.Position = 0;
            byte[] result = new byte[memStream.Length];
            memStream.Read(result, 0, result.Length);

            // Close and release the streams
            memStream.Close();
            cryptStream.Close();

            // Hand back the encrypted buffer
            return result;
        }
    }
}
