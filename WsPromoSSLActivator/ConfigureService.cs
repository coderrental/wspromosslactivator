﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using WsPromoSSLActivator.Helpers;
using System.Configuration;

namespace WsPromoSSLActivator
{
    internal static class ConfigureService
    {
        //private static cTripleDes _des;
        internal static void Configure()
        {
            byte[] des_key = { 20, 21, 24, 32, 1, 11, 12, 22, 23, 13, 14, 34, 89, 31, 56, 15, 16, 17, 18, 19, 35, 59, 2, 3 };
            byte[] des_iv = { 8, 7, 6, 5, 4, 3, 2, 1 };
            cTripleDes des = new cTripleDes(des_key, des_iv);

            string dnnConnString = ConfigurationManager.ConnectionStrings["DnnConnection"].ConnectionString;
            string cspMasterConnString = ConfigurationManager.ConnectionStrings["CspMasterConnection"].ConnectionString;

            string queryFile = ConfigurationManager.AppSettings["QueryFilePath"];
            int serviceInterval = int.Parse(ConfigurationManager.AppSettings["ServiceInterval"]);

            HostFactory.Run(configure =>
            {
                configure.Service<WsPromoSSLActivatorService>(service =>
                {
                    service.ConstructUsing(s => new WsPromoSSLActivatorService(des, dnnConnString, cspMasterConnString, queryFile, serviceInterval));
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                //Setup Account that window service use to run.  
                configure.RunAsLocalSystem();
                configure.SetServiceName("CSPPromoSSLActivator");
                configure.SetDisplayName("CSPPromoSSLActivator");
                configure.SetDescription("CSPPromoSSLActivator");
            });
        }
    }
}
