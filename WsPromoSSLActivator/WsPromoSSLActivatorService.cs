﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using log4net.Core;
using Newtonsoft.Json;
using WsPromoSSLActivator.DataContext;
using WsPromoSSLActivator.Helpers;


namespace WsPromoSSLActivator
{
    public class WsPromoSSLActivatorService
    {
        private readonly cTripleDes _tripleDes;
        private readonly string _dnnConnString;
        private readonly string _cspMasterConString;
        private readonly string _queryFile;
        private string _sqlQuery = "";
        private List<string> _projectList; 
        private readonly int _serviceInterval;
        private Task _proccessCacheQueueTask;
        private CancellationTokenSource _cancellationTokenSource;
        readonly ILog _log = LogManager.GetLogger(typeof(WsPromoSSLActivatorService));
        public Dictionary<int, CancellationTokenSource> TaskCancellationTokenSources = new Dictionary<int, CancellationTokenSource>();

        public WsPromoSSLActivatorService(cTripleDes tripleDes, string dnnConnString, string cspMasterConString, string queryFile, int serviceInterval)
        {
            _tripleDes = tripleDes;
            _dnnConnString = dnnConnString;
            _cspMasterConString = cspMasterConString;
            _queryFile = queryFile;
            _serviceInterval = serviceInterval;
        }

        public void Start()
        {
            _log.Info("WsPromoSSLActivatorService started");
            _cancellationTokenSource = new CancellationTokenSource();
            _proccessCacheQueueTask = Task.Run(() => DoSSLActivating(_cancellationTokenSource.Token));
        }
        public void Stop()
        {
            try
            {
                foreach (KeyValuePair<int, CancellationTokenSource> taskCancellationTokenSource in TaskCancellationTokenSources)
                {
                    taskCancellationTokenSource.Value.Cancel();
                }
            }
            catch (Exception e)
            {
            }
            
            _cancellationTokenSource.Cancel();

            try
            {
                _proccessCacheQueueTask.Wait();
            }
            catch (Exception e)
            {
                _log.Debug("WsPromoSSLActivatorService stopped");
            }
        }

        public async Task DoSSLActivating(CancellationToken token)
        {
            while (true)
            {
                try
                {
                    if (!File.Exists(_queryFile))
                    {
                        _log.Error("Query file not found");
                        continue;
                    }
                    _sqlQuery = File.ReadAllText(_queryFile);
                    using (DnnDataContext dnnContext = new DnnDataContext(_dnnConnString))
                    {
                        var promoProjects =
                            dnnContext.CSPPromotionSSLActivates.Where(x => x.IsProcessed == false).ToList();
                        for (int i = 0; i < promoProjects.Count; i++)
                        {
                            promoProjects[i].IsProcessed = true;
                            dnnContext.SubmitChanges(ConflictMode.FailOnFirstConflict);
                            _log.Info("Start to enable SSL for " + promoProjects[i].Project);
                            using (CspMasterDataContext masterContext = new CspMasterDataContext(_cspMasterConString))
                            {
                                var cspProj =
                                    masterContext.Connections.FirstOrDefault(
                                        x => x.Project.ToLower() == promoProjects[i].Project.ToLower());
                                if (cspProj == null) return;

                                var prjSqlQuery = string.Format(_sqlQuery, promoProjects[i].Project);

                                var cspDbConnString = _tripleDes.Decrypt(cspProj.Connections);
                                var cspDbConn = new SqlConnection(cspDbConnString);
                                SqlCommand cmd = new SqlCommand(prjSqlQuery, cspDbConn);
                                cspDbConn.Open();
                                cmd.ExecuteNonQuery();
                                cspDbConn.Close();
                            }
                        }
                        

                        /*Parallel.ForEach(promoProjects.AsParallel(), prj =>
                        {
                            using (CspMasterDataContext masterContext = new CspMasterDataContext(_cspMasterConString))
                            {
                                var cspProj =
                                    masterContext.Connections.FirstOrDefault(
                                        x => x.Project.Equals(prj.Project, StringComparison.InvariantCultureIgnoreCase));
                                if (cspProj == null) return;

                                _sqlQuery = string.Format(_sqlQuery, prj.Project);
                                var cspDbConnString = _tripleDes.Decrypt(cspProj.Connections);
                                var cspDbConn = new SqlConnection(cspDbConnString);
                                SqlCommand cmd = new SqlCommand(_sqlQuery, cspDbConn);
                                cspDbConn.Open();
                                cmd.ExecuteNonQuery();
                                cspDbConn.Close();
                            }
                        });*/
                    }
                }
                catch (Exception e)
                {
                    _log.Error(e.StackTrace);
                }
                await Task.Delay(TimeSpan.FromMilliseconds(_serviceInterval), token);
            }
        }
        
    }
}
